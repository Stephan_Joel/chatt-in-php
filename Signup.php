<!DOCTYPE html>
<html>
<head>
    <title>Create an account</title>
    <meta cjarset="utf-8"/>
    <meta http-equiv="X-UA-Compaible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"href="https://fonts.googleapis.com/css?family=Robot|Courgette|Pacifico:400,700">
    <link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/signup.css">
</head>
<body>
    <div class="signup-form">
        <form action="signup_user.php" method="post">
            <div class="form-header">
                <h2>Sign Up</h2>
                <p>Fill this form and start chating with your friends.</p>
            </div>
            <div class="form-group">
                <label for="email">Username</label>
                <input type="text" class="form-control" name="username" placeholder="username" autocomplete="off" >
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off" required>
            </div>
            <div class="form-group">
                <label for="email">Email Address</label>
                <input type="email" class="form-control" name="user_email" placeholder="example@gmail.com" autocomplete="off" required>
            </div>
            <div class="form-group">
                <label for="country">Country</label>
                     <select id="country" class="form-control" name="user_country" required>
                            <option disabled="">Select a country</option>
                            <option value="cameroon">Cameroon</option>
                            <option value="nigeria">Nigeria</option>
                            <option value="egypte">Egypte</option>
                            <option value="ghana">Ghana</option>
                            <option value="mali">Mali</option>
                            <option value="congo">Congo</option>
                     </select>
            </div>
            <div class="form-group">
                <label for="gender">Gender</label>
                     <select id="gender" class="form-control" name="user_gender" required>
                            <option disabled="">Select your gender</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                     </select>
            </div>
            <div class="form-group">
                <label for="" class="checkbox-inline"><input type="checkbox" required>I accept the <a href="#">Terms of use </a>&amp; <a href="#">Privacy Policy</a> </label>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn btn-block btn-lg" name="sign_up">Sign Up</button>
                <?php //include("signup_user.php"); ?>
            </div>
            </div>
            
           
        </form>
        <div class="text-center small" style="color:#67428B; font-weight:bold;">Already have an account ?<a href="index.php">SignIn here</a></div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script> 
</body>
</html>