<?php
session_start();
include("includes/connection.php");
include("includes/header.php");
if(!isset($_SESSION['user_email'])){
    header("Location:index.php");
}else{ ?>
<!DOCTYPE html>
<html>
<head>
    <title>Account Settings</title>
    <meta cjarset="utf-8"/>
    <meta http-equiv="X-UA-Compaible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="row">
        <div class="col-sm-2">
        
        </div>
        <?php
            $user = $_SESSION['user_email'];
            $get_user = "SELECT * FROM users WHERE user_email ='$user'";
            $run_user = mysqli_query($con,$get_user);
            $row = mysqli_fetch_array($run_user);

            $user_name = $row['user_name'];
            $user_pass = $row['user_pass'];
            $user_email = $row['user_email'];
            $user_profile = $row['user_profile'];
            $user_country = $row['user_country'];
            $user_gender = $row['user_gender'];
            $user_id = $row['user_id'];
        ?>
        <div class="col-sm-8">
            <form action="" method="post" enctype="multipart/form-data">
                <table class="table table-bordered table-hover">
                    <tr align="center">
                        <td colspan="6" class="active"><h2>Change Account Settings</h2></td>
                    </tr>
                    <tr>
                        <td style="font-weight:bold;">Change your Username</td>
                        <td>
                            <input type="text" name="u_name" class="form-control" required value="<?=$user_name;?>"/>
                        </td>
                    </tr>
                    <tr><td><td><a href="upload.php" class="btn btn-default" style="text-decoration:none; font-size:15px;">
                    <i class="fa fa-user" aria-hidden="true"></i>Change profile</a></td></td></tr>
                    <tr>
                        <td style="font-weight:bold;">Change your Email</td>
                        <td>
                            <input type="email" name="u_email" class="form-control" required value="<?=$user_email;?>"/>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight:bold;">Country</td>
                        <td>
                            <select name="u_country" id="" class="form-control">
                            <option value="<?=$user_country;?>"><?=$user_country;?></option>
                            <option value="cameroon">Cameroon</option>
                            <option value="nigeria">Nigeria</option>
                            <option value="egypte">Egypte</option>
                            <option value="ghana">Ghana</option>
                            <option value="mali">Mali</option>
                            <option value="congo">Congo</option>
                            
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-weight:bold;">Gender</td>
                        <td>
                            <select name="u_gender" id="" class="form-control">
                            <option value="<?=$user_gender;?>"><?=$user_gender;?></option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                            </select>
                        </td>
                    </tr>

                    <tr>    
                        <td style="font-weight:bold;">Forgotten Pasword ?</td>
                        <td>
                        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">Forgotten Password</button>
                                <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Forgotten Password</h4>
                                        </div>

                                        <div class="modal-body">
                                            <form action="recovery.php?id=<?=$user_id;?>" method="post" id="f">
                                                <strong>What is your School Best Friend Name ?</strong>
                                                <textarea name="content" id="" cols="83" rows="4"
                                                 class="form-control" placeholder="Someone" style="resize: none;"></textarea>
                                                 <input type="submit" class="btn btn-default" type="submit" name="sub"
                                                  value="Submit" style="width:100px;"> <br> <br>
                                                  <pre>Answer the above question we will ask you this
                                                   question if you forget yourPassword.</pre>
                                                   <br> <br>
                                            </form>
                                            <?php
                                                if(isset($_POST['sub'])){
                                                    $bfn = htmlentities($_POST['content']);
                                                    if($bfn==''){
                                                        echo "<script>alert('please enter something.') </script>";
                                                        echo "<script>window.open('account_settings.php','_self')</script>";
                                                        exit();
                                                       
                                                    }else{
                                                        $update = "UPDATE users set forgotten_answer='$bfn' WHERE user_email='$user'";
                                                        $run = mysqli_query($con,$update);
                                                        var_dump($update);
                                                        if($run){
                                                            echo "<script>alert('Working...') </script>";
                                                            echo "<script>window.open('account_settings.php','_self')</script>";
                                                            
                                                        }else{
                                                            echo "<script>alert('Error while updating information') </script>";
                                                            echo "<script>window.open('account_settings.php','_self')</script>";
                                                          
                                                        }
                                                    }
                                                }
                                            
                                            ?>
                                        </div>
                                        <div class="modal-footer">
                                                <button class="btn btn-default" type="button"
                                                data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                             </div>
                        
                    </tr>
                    <tr><td><td><a href="change_password.php" class="btn btn-default"
                    style="text-decoration:none font-size:15px;"><i class="fa fa-key fa-fw" aria-hidden="true"></i>Change Password</a></td></td></tr>
                    <tr align="center">
                        <td colspan="6">
                            <input type="submit" value="Update" name="update" class="btn btn-info">                    
                        </td>
                    </tr>
                </table>
            </form>
            <?php
            if(isset($_POST['update'])){
                $user_name = htmlentities($_POST['u_name']);
                $email = htmlentities($_POST['u_email']);
                $u_gender = htmlentities($_POST['u_gender']);
                $u_contry = htmlentities($_POST['u_country']);

                $update = "UPDATE users SET user_name='$user_name', user_email='$email',
                user_country='$u_contry', user_gender='$u_gender'WHERE user_email='$user' ";
                $run= mysqli_query($con,$update);
                if($run){
                    echo "ok";
                    echo "<script>window.open('account_settings.php','_self')</script>";
                }
               

            }

            ?>
        </div>
        <div class="col-sm-2">
        
        </div>
    </div>
    <?php } ?>
</body>
</html>