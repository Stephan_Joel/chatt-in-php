<?php
session_start();
include("../includes/connection.php");
include("../includes/find_friends_function.php");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Search for friends</title>
    <meta cjarset="utf-8"/>
    <meta http-equiv="X-UA-Compaible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"href="https://fonts.googleapis.com/css?family=Robot|Courgette|Pacifico:400,700">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/signin.css">
    <link rel="stylesheet" type="text/css" href="../css/find_people.css">
</head>
<body>
    <nav class="navbar navbar-inverse">
        <a href="#" class="navbar-brand">
            <?php
            $user = $_SESSION['user_email'];
            $get_user = "SELECT * FROM users WHERE user_email='$user'";
            $run_user = mysqli_query($con,$get_user);
            $row = mysqli_fetch_array($run_user);
            $user_name = $row['user_name'];
            echo "<a class='navbar-brand' href='../home.php?user_name=$user_name'>MyChat</a>";
            ?>
        </a>
             <ul class="navbar-nav">
                <li><a href="../account_settings.php" style="color:white; text-decoration:none; font-size:20px;"><i class="fa fa-cog" aria-hidden="true"></i></i>setting</a></li>
            </ul>
    </nav> <br>
     <div class="row">
        <div class="col-sm-4">

        </div>
        <div class="col-sm-4">
            <form action="" class="search">
                <input type="text" name="search_query" placeholder="Search friends" autocomplete="off" required>
                <button class="btn" type="submit" name="search_btn">Search</button>
            </form>
        </div>
        <div class="col-sm-4">

        </div>

     </div> <br> 
     <?php search_user();?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script> 
</body>
</html>