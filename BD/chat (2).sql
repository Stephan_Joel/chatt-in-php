-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 01 juil. 2021 à 18:32
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `chat`
--

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(250) NOT NULL,
  `user_pass` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_profile` varchar(250) NOT NULL,
  `user_country` varchar(250) NOT NULL,
  `user_gender` varchar(250) NOT NULL,
  `forgotten_answer` varchar(250) DEFAULT NULL,
  `log_in` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_pass`, `user_email`, `user_profile`, `user_country`, `user_gender`, `forgotten_answer`, `log_in`) VALUES
(1, 'stephan', '698587633', 'stephanjoel6985@gmail.com', 'images/image3.png', 'cameroon', 'male', NULL, 'Offline'),
(2, 'pacos', '675530326', 'paco@gmail.com', 'images/image2.png', 'cameroon', 'male', 'kontio', 'Offline'),
(3, 'kontio', '699859601', 'kontio@gmail.com', 'images/image2.png', 'cameroon', 'male', NULL, 'Offline'),
(4, 'yanick', '698587633', 'yanick@gmail.com', 'images/image2.png', 'cameroon', 'male', NULL, 'Offline');

-- --------------------------------------------------------

--
-- Structure de la table `users_chat`
--

DROP TABLE IF EXISTS `users_chat`;
CREATE TABLE IF NOT EXISTS `users_chat` (
  `msg_id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_username` varchar(250) NOT NULL,
  `receiver_username` varchar(250) NOT NULL,
  `msg_content` text DEFAULT NULL,
  `msg_status` text DEFAULT NULL,
  `msg_date` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`msg_id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users_chat`
--

INSERT INTO `users_chat` (`msg_id`, `sender_username`, `receiver_username`, `msg_content`, `msg_status`, `msg_date`) VALUES
(1, 'stephan', 'stephan', 'sqddsqddsdddsdsqd', 'read', '2021-06-04 18:52:32'),
(2, 'stephan', 'kontio', 'hey bro', 'read', '2021-06-04 19:53:33'),
(3, 'stephan', 'kontio', 'hey bro', 'read', '2021-06-04 19:57:44'),
(4, 'kontio', 'stephan', 'yes how are you', 'read', '2021-06-04 20:00:01'),
(5, 'kontio', 'stephan', 'yes', 'read', '2021-06-04 20:00:15'),
(6, '', 'stephan', 'yes', 'read', '2021-06-04 20:03:03'),
(7, 'stephan', 'kontio', 'ont dit quoi ?', 'read', '2021-06-04 20:05:27'),
(8, 'stephan', 'kontio', 'ont dit quoi ?', 'read', '2021-06-04 20:05:30'),
(9, 'stephan', 'kontio', 'ont dit quoi ?', 'read', '2021-06-04 20:26:18'),
(10, 'stephan', 'kontio', 'ont dit quoi ?', 'read', '2021-06-04 20:27:02'),
(11, 'stephan', 'kontio', 'ont dit quoi ?', 'read', '2021-06-04 20:27:24'),
(12, 'stephan', 'kontio', 'ont dit quoi ?', 'read', '2021-06-04 20:50:04'),
(13, 'stephan', 'kontio', 'ont dit quoi ?', 'read', '2021-06-04 20:50:18'),
(14, 'stephan', 'kontio', 'ont dit quoi ?', 'read', '2021-06-04 20:50:55'),
(15, 'stephan', 'kontio', 'ont dit quoi ?', 'read', '2021-06-04 21:00:25'),
(16, 'stephan', 'kontio', 'yo bro comment tu va', 'read', '2021-06-05 11:47:13'),
(17, 'stephan', 'kontio', 'yo bro comment tu va', 'read', '2021-06-05 11:47:19'),
(18, 'kontio', 'stephan', 'tranquille et toi', 'read', '2021-06-05 11:47:41'),
(19, 'stephan', 'kontio', 'yo bro comment tu va', 'read', '2021-06-05 11:47:45'),
(20, 'kontio', 'stephan', 'tranquille et toi', 'read', '2021-06-05 11:53:40'),
(21, 'stephan', 'kontio', 'hey whatsupp ', 'read', '2021-06-17 17:30:18'),
(22, '', 'kontio', 'hey whatsupp ', 'unread', '2021-06-17 17:32:10');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
