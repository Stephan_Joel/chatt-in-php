<!DOCTYPE html>
<html>
<head>
    <title>Login to your Account</title>
    <meta cjarset="utf-8"/>
    <meta http-equiv="X-UA-Compaible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"href="https://fonts.googleapis.com/css?family=Robot|Courgette|Pacifico:400,700">
    <link rel="stylesheet" type="text/css" href="Bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/signin.css">
</head>
<body>
    <div class="signin-form">
        <form action="" method="post">
            <div class="form-header">
                <h2>Sign In</h2>
                <p>Login to MyChat</p>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="email" placeholder="example@gmail.com" autocomplete="off" required>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off" required>
            </div>
            <div class="small">Forgot password? <a href="forgot_pass.php">Click here</a></div> <br>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn btn-block btn-lg" name="sign_in">Sign In</button>
                <?php include("signin_user.php"); ?>
            </div>
        </form>
        <div class="text-center small" style="color:#ffffff; font-weight:bold; font-size:18px; background:#00cb82;">Don't have an account?<a href="Signup.php">Create one</a></div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script> 
</body>
</html>